import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        boolean guepardoAlive=true;
        boolean tortugaAlive=true;
        boolean liebreAlive=true;
        double duration = 1*Math.pow(10,9);

        Thread guepardo =new Animal( "Guepardo", duration);
        Thread tortuga =new Animal( "Tortuga", duration );
        Thread liebre =new Animal( "Guepardo", duration );

        guepardo.start();
        tortuga.start();
        liebre.start();

        StringBuilder st = new StringBuilder();
        st.append("Resultados:");

        while(guepardoAlive || tortugaAlive || liebreAlive){
            if(guepardoAlive && !guepardo.isAlive()){
                guepardoAlive=false;
                st.append("\n-Guepardo");
            }
            if(tortugaAlive && !tortuga.isAlive()){
                tortugaAlive=false;
                st.append("\n-Tortuga");
            }
            if(liebreAlive && !liebre.isAlive()){
                liebreAlive=false;
                st.append("\n-Liebre");
            }
        }

        System.out.println(st);

    }
}
