import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Animal extends Thread {
    private String name;

    private long starts;
    private double duration;

    public Animal(String name, double duration) {
        this.name=name;
        this.duration=duration;
    }

    @Override
    public void run() {
        starts = System.nanoTime();
        while(System.nanoTime()-starts<=duration) {
            System.out.println(name + ": " + (System.nanoTime() - starts));
        }
    }
}
