import java.util.concurrent.Semaphore;

public class Yes extends Thread {

    Semaphore semaforo;

    public Yes(Semaphore semaforo) {
        this.semaforo = semaforo;
    }

    public void run() {
        try {
            this.semaforo.acquire();

            for (int i = 0; i < 10; i++) {
                System.out.println("Yes");
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {

                }
                Thread.yield();
            }
            this.semaforo.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
