import java.util.concurrent.Semaphore;

public class Main {

    /**
     * Escribe una aplicación que tenga dos hilos. Un hilo escribirá “SI” mientas que el otro
     * escribirá “NO”. Cada hilo escribirá 10 veces su mensaje. La salida deberá de ser la
     * siguiente:
     * SI SI SI SI SI SI SI SI SI SI NO NO NO NO NO NO NO NO NO NO
     * @param args
     */
    public static void main(String[] args){
        Semaphore semaforo =new Semaphore(1);
        Thread tYes= new Yes(semaforo);
        Thread tNo= new No(semaforo);
        tYes.start();
        tNo.start();
    }
}
