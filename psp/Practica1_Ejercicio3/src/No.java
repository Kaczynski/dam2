import java.util.concurrent.Semaphore;

public class No extends Thread{

    Semaphore semaforo;

    public No(Semaphore semaforo) {
        this.semaforo = semaforo;
    }
    public void run() {
        try {
            this.semaforo.acquire();

            for (int i = 0; i<10 ; i++) {
            System.out.println("No");
            try {
                Thread.sleep(1000);
            }catch (Exception e){

            }
            Thread.yield();
        }
            this.semaforo.release();
    } catch (InterruptedException e) {
        e.printStackTrace();
    }

    }
}
