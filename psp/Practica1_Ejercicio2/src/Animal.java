import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class Animal extends Thread {
    private String name;

    Results semaforo;
    private int duration;

    public Animal(String name, Results semaforo, int duration) {
        this.name=name;
        this.semaforo=semaforo;
        this.duration=duration;
    }

    @Override
    public void run() {

        for(int i = 0; i<duration;i++){
            System.out.println(name);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        semaforo.registro(name);

    }
}
