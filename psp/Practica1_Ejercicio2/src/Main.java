import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

public class Main {



    public static void main(String[] args) throws InterruptedException {
        Results semaforo = new Results();
        String[] animales= new String[]{
                "Guepardo","Tortuga","Liebre"
        };
        ArrayList<Animal> threads = new ArrayList<>();
        for(String g : animales) {
            threads.add(new Animal( g, semaforo,30 ) );
        }



        for(Animal a: threads){
            a.start();
        }
        for (Animal a: threads) {
            a.join();
        }

        System.out.println("Resultados::");
        for(String s : semaforo.orden) {
            System.out.println(s);
        }

    }
}
