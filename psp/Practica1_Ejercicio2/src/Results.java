import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Results {

    Semaphore semaforo;
    ArrayList<String> orden;
    public Results() {
        semaforo = new Semaphore(1);
        orden = new ArrayList<>();
    }

    public void registro(String nombre){
        try {
            semaforo.acquire();
            orden.add(nombre);
            semaforo.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
