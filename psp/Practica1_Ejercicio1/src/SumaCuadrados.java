public class SumaCuadrados extends Thread {
    private int[] array;

    public SumaCuadrados(int[] array) {
        this.array = array;
    }
    public void run(){
        int suma=0;
        for(int i : array){
            suma+=Math.pow(i,2);
        }
        System.out.println("SumaCuadrados: "+suma);
    }
    }

