public class Suma extends Thread{
    private int[] array;

    public Suma(int[] array) {
        this.array = array;
    }

        public void run(){
        int suma=0;
            for(int i : array){
                suma+=i;
            }
            System.out.println("Suma: "+suma);
        }

}
