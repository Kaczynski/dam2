import java.util.Arrays;

public class Main {

    public static void main(String[] args){
        int [] array= {5,6,8,2,6,4};
        Thread t1 = new Suma(array);
        Thread t2 = new Media(array);
        Thread t3 = new SumaCuadrados(array);
        System.out.print("Array utilizado: ");
        System.out.println(Arrays.toString(array));
        t1.start();
        t2.start();
        t3.start();
    }
}
