
import java.awt.*;
import javax.swing.*;
import java.beans.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main
        implements
        PropertyChangeListener {

    private Vista panel;
    private Task task;

    class Task extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {

            int progress = 0;

            setProgress(0);
            for (int i = 0; i < 10; i++) {
                try {
                    Thread t = new Hilo();
                    t.start();
                    t.join();
                    setProgress(Math.min((i+1)*10, 100));
                } catch (InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            return null;
        }


        @Override
        public void done() {
            Toolkit.getDefaultToolkit().beep();
            panel.panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        }
    }

    public Main() {
        createAndShowGUI();
        panel.progressBar1.setValue(0);
        panel.progressBar1.setStringPainted(true);
        runTasks();
    }


    private void runTasks(){

        panel.panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        task = new Task();
        task.addPropertyChangeListener(this);
        task.execute();
    }


    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress".equals(evt.getPropertyName())) {
            int progress = (Integer) evt.getNewValue();
            panel.progressBar1.setValue(progress);
        }
    }


    private void createAndShowGUI() {

        JFrame frame = new JFrame("Barra de progreso con 10 threads");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.panel = new Vista();
        frame.setContentPane(panel.panel);
        frame.pack();
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Main();
            }
        });
    }
}