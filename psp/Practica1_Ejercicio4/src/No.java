import java.util.concurrent.Semaphore;

public class No extends Thread {

    Semaphore semaforoEspera, semaforoEjecucion;


    public No(Semaphore semaforoEjecucion, Semaphore semaforoEspera) {
        this.semaforoEjecucion = semaforoEjecucion;
        this.semaforoEspera = semaforoEspera;

    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                this.semaforoEspera.acquire(); // espera y obtiene el semaforo "espera"
                System.out.println("no");

                try {
                    Thread.sleep(100);
                } catch (Exception e) {

                }
                this.semaforoEjecucion.release(); // Libera el semaforo de Ejecucion


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
