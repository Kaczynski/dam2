import java.util.concurrent.Semaphore;

public class Yes extends Thread{
    Semaphore semaforoEspera, semaforoEjecucion;


    public Yes(Semaphore semaforoEjecucion, Semaphore semaforoEspera) {
        this.semaforoEjecucion = semaforoEjecucion;
        this.semaforoEspera = semaforoEspera;

    }
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {

                this.semaforoEjecucion.acquire();
                System.out.println("Yes");

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {

                }
                this.semaforoEspera.release();



            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
