import java.util.concurrent.Semaphore;

public class Main {

    /***
     * Escribe una aplicación que tenga dos hilos. Un hilo escribirá “SI” mientas que el otro
     * escribirá “NO”. Cada hilo escribirá 10 veces su mensaje. La salida deberá de ser la
     * siguiente:
     * SI NO SI NO SI NO SI NO SI NO SI NO SI NO SI NO SI NO SI NO
     * @param args
     */
    public static void main(String[] args){
        // 2 semaforos para intelcalar la ejecucion
        Semaphore semaforoEjecucion = new Semaphore(1);
        Semaphore semaforoEspera = new Semaphore(1);

        // Adquirimos el semaforo para que empieze en yes
        try {
            semaforoEspera.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // instancia de los dos hilos
        Thread tYes= new Yes(semaforoEjecucion, semaforoEspera);
        Thread tNo= new No(semaforoEjecucion,semaforoEspera);
        // Lanzamos los dos hilos
        tYes.start();
        tNo.start();

        //Espoeramos a que terminen los hilos
        try {
            tYes.join();
            tNo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Terminado");
    }
}
